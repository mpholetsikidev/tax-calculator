/**
 * 
 */
package com.pysbergate;

/**
 * @author mpho.letsiki
 *
 */
public class Calculator {

  public static void main(String[] args) {
    System.out.println("Tax Payable is: " + getAnnualTax(150000, 25));
    System.out.println("Tax Payable is: " + getMonthlyTax(12500, 25));
  }

  /**
   * @param salary
   */
  private static double getAnnualTax(double salary, int age) {
    double totalTax = 0.00;

    for (TaxTable taxTable : TaxTable.getTaxTable()) {
      if (salary >= taxTable.getAmountFrom()) {
        if (salary <= taxTable.getAmountTo()) {
          totalTax = totalTax + taxTable.getInitialTax();
          totalTax = salary * (taxTable.getTaxRate() / 100);
          salary = salary - taxTable.getAmountTo();

          if (salary < 0) {
            break;
          }
        }
      }
    }

    return totalTax - getTaxRebate(age);
  }

  /**
   * @param salary
   */
  private static double getMonthlyTax(double salary, int age) {
    double totalTax = 0.00;

    for (TaxTable taxTable : TaxTable.getTaxTable()) {
      if (salary >= taxTable.getAmountFrom()) {
        if (salary <= taxTable.getAmountTo()) {
          totalTax = totalTax + taxTable.getInitialTax();
          totalTax = salary * (taxTable.getTaxRate() / 100 / 12);
          salary = salary - taxTable.getAmountTo();

          if (salary < 0) {
            break;
          }
        }
      }
    }
    return totalTax + (getTaxRebate(age) / 12);

    // return totalTax - getTaxRebate(age);
  }

  /**
   * @param age
   */
  private static double getTaxRebate(int age) {
    if (age < 65) {
      return TaxRebate.getPrimaryRebate();
    }
    else if (age >= 65 && age < 75) {
      return TaxRebate.getSecondaryRebate();
    }
    else if (age >= 75) {
      return TaxRebate.getTertiaryRebate();
    }
    return 0;
  }
}