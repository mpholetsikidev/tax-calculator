/**
 * 
 */
package com.pysbergate;

/**
 * @author mpho.letsiki
 *
 */
public class TaxRebate {

  private static double primaryRebate = 12726;

  private static double secondaryRebate = 7110;

  private static double tertiaryRebate = 2367;

  /**
   * @return the primaryRebate
   */
  public static double getPrimaryRebate() {
    return primaryRebate;
  }

  /**
   * @return the secondaryRebate
   */
  public static double getSecondaryRebate() {
    return secondaryRebate;
  }

  /**
   * @return the tertiaryRebate
   */
  public static double getTertiaryRebate() {
    return tertiaryRebate;
  }
}