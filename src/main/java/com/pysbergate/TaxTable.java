/**
 * 
 */
package com.pysbergate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mpho.letsiki
 *
 */
public class TaxTable {

  private double amountFrom;

  private double amountTo;

  private double taxRate;

  private double initialTax;

  public TaxTable() {
  }

  public TaxTable(double amountFrom, double amountTo, double taxRate, double initialTax) {
    this.amountFrom = amountFrom;
    this.amountTo = amountTo;
    this.taxRate = taxRate;
    this.initialTax = initialTax;
  }

  public static List<TaxTable> getTaxTable() {
    List<TaxTable> taxTable = new ArrayList<TaxTable>();
    TaxTable row1 = new TaxTable(0, 174550, 18, 0);
    TaxTable row2 = new TaxTable(174551, 272700, 25, 31419);
    TaxTable row3 = new TaxTable(272701, 377450, 30, 55957);
    TaxTable row4 = new TaxTable(377451, 528000, 35, 87382);
    TaxTable row5 = new TaxTable(528001, 673100, 38, 140074);
    TaxTable row6 = new TaxTable(673101, 999999999, 40, 195212);

    taxTable.add(row1);
    taxTable.add(row2);
    taxTable.add(row3);
    taxTable.add(row4);
    taxTable.add(row5);
    taxTable.add(row6);
    return taxTable;
  }

  /**
   * @return the amountFrom
   */
  public double getAmountFrom() {
    return amountFrom;
  }

  /**
   * @return the amountTo
   */
  public double getAmountTo() {
    return amountTo;
  }

  /**
   * @return the taxRate
   */
  public double getTaxRate() {
    return taxRate;
  }

  /**
   * @return the initialTax
   */
  public double getInitialTax() {
    return initialTax;
  }
}